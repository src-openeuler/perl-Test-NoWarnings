Name:           perl-Test-NoWarnings
Version:        1.04
Release:        18
Summary:        Make sure you didn't emit any warnings while testing
License:        LGPL-2.1-or-later
URL:            https://metacpan.org/release/Test-NoWarnings
Source0:        https://www.cpan.org/authors/id/A/AD/ADAMK/Test-NoWarnings-%{version}.tar.gz
BuildArch:      noarch
BuildRequires:  make perl-generators perl-interpreter perl(:VERSION) >= 5.6
BuildRequires:  perl(ExtUtils::MakeMaker) >= 6.76 perl(strict)
BuildRequires:  perl(Carp) perl(Exporter) perl(Test::Builder) >= 0.86
BuildRequires:  perl(warnings) perl(vars)
BuildRequires:  perl(Test::More) >= 0.47 perl(Test::Tester) >= 0.107

%description
In general, your tests shouldn't produce warnings. This modules causes
any warnings to be captured and stored. It automatically adds an extra
test that will run when your script ends to check that there were no
warnings. If there were any warings, the test will give a "not ok" and
diagnostics of where, when and what the warning was, including a stack
trace of what was going on when the it occurred.

If some of your tests *are supposed to* produce warnings then you should be
capturing and checking them with the Test::Warn manpage, that way the
Test::NoWarnings manpage will not see them and so not complain.

The test is run by an 'END' block in Test::NoWarnings. It will not be run
when any forked children exit.

%package_help


%prep
%autosetup -p1 -n Test-NoWarnings-%{version}

%build
perl Makefile.PL NO_PACKLIST=1 INSTALLDIRS=vendor
%make_build

%install
make pure_install DESTDIR=%{buildroot}
%{_fixperms} %{buildroot}/*

%check
make test

%files
%license LICENSE
%{perl_vendorlib}/*

%files help
%doc Changes README
%{_mandir}/man3/*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 1.04-18
- drop useless perl(:MODULE_COMPAT) requirement

* Tue Jan 14 2020 xuxijian<xuxijian@huawei.com> - 1.04-17
- Delete a buildRequire

* Tue Dec 3 2019 mengxian <mengxian@huawei.com> - 1.04-16
- Package init
